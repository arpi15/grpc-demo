package edu.ubb.weblive.todoapp.grpc;

import com.google.protobuf.Empty;
import edu.ubb.weblive.todoapp.*;
import edu.ubb.weblive.todoapp.model.Todo;
import edu.ubb.weblive.todoapp.service.TodosManager;
import edu.ubb.weblive.todoapp.service.TodosManagerFactory;
import io.grpc.stub.StreamObserver;

import java.util.Set;
import java.util.stream.Collectors;

public class TodoGrpcImpl extends TodoGrpc.TodoImplBase {

    private final TodosManager todosManager;

    public TodoGrpcImpl() {
        this.todosManager = TodosManagerFactory.getTodosManager();
    }

    @Override
    public void getAll(Empty request, StreamObserver<GetTodosReply> responseObserver) {
        Set<Todo> todos = todosManager.findAll();

        GetTodoReply.Builder todoBuilder = GetTodoReply.newBuilder();
        Iterable<GetTodoReply> todoReplies = todos.stream()
                .map(todo -> todoBuilder.setId(todo.getId()).setDescription(todo.getDescription()).build())
                .collect(Collectors.toList());

        GetTodosReply.Builder todoRepliesBuilder = GetTodosReply.newBuilder();
        GetTodosReply getTodosReply = todoRepliesBuilder
                .addAllTodos(todoReplies)
                .build();

        responseObserver.onNext(getTodosReply);
        responseObserver.onCompleted();
    }

    @Override
    public void get(GetTodoRequest request, StreamObserver<GetTodoReply> responseObserver) {
        Integer id = request.getId();

        Todo todo = todosManager.findById(id);
        if (todo == null) {
            responseObserver.onError(new Exception("Todo with this ID not found."));
            return;
        }

        GetTodoReply getTodoReply = GetTodoReply.newBuilder()
                .setId(todo.getId())
                .setDescription(todo.getDescription())
                .build();

        responseObserver.onNext(getTodoReply);
        responseObserver.onCompleted();
    }

    @Override
    public void post(PostTodoRequest request, StreamObserver<Empty> responseObserver) {
        String description = request.getDescription();
        todosManager.create(description);
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void put(PutTodoRequest request, StreamObserver<Empty> responseObserver) {
        Integer id = request.getId();
        String description = request.getDescription();
        todosManager.update(id, description);
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void delete(DeleteTodoRequest request, StreamObserver<Empty> responseObserver) {
        Integer id = request.getId();
        todosManager.delete(id);
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }
}
