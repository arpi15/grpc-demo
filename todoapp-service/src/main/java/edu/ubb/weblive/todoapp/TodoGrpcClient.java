package edu.ubb.weblive.todoapp;

import com.google.protobuf.Empty;
import io.grpc.Channel;
import io.grpc.Grpc;
import io.grpc.InsecureChannelCredentials;
import io.grpc.ManagedChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class TodoGrpcClient {

    private static final Logger LOG = LoggerFactory.getLogger(TodoGrpcClient.class);

    private final TodoGrpc.TodoBlockingStub blockingStub;

    public TodoGrpcClient(Channel channel) {
        blockingStub = TodoGrpc.newBlockingStub(channel);
    }

    private void simulateExampleServerFlow() {
        LOG.info("Simulating example server flow.");
        LOG.info("--------------------------------------------------\n");

        LOG.info("Listing all todos...");
        GetTodosReply getTodosReply = blockingStub.getAll(Empty.newBuilder().build());
        getTodosReply.getTodosList().forEach(todo -> LOG.info("Todo (id: " + todo.getId()
                + ", description: " + todo.getDescription() + ")"));
        LOG.info("--------------------------------------------------\n");

        LOG.info("Creating new todos...");
        LOG.info("--------------------------------------------------\n");
        blockingStub.post(createTodo("Buy milk"));
        blockingStub.post(createTodo("Clean up at house"));
        blockingStub.post(createTodo("Learn for maths"));
        blockingStub.post(createTodo("Go to the gym"));
        blockingStub.post(createTodo("Call mom"));
        blockingStub.post(createTodo("Prepare for the exam"));
        blockingStub.post(createTodo("Go to the doctor"));
        blockingStub.post(createTodo("Buy a new phone"));

        LOG.info("Listing all todos...");
        getTodosReply = blockingStub.getAll(Empty.newBuilder().build());
        getTodosReply.getTodosList().forEach(todo -> LOG.info("Todo (id: " + todo.getId()
                + ", description: " + todo.getDescription() + ")"));
        LOG.info("--------------------------------------------------\n");

        int idRequested = 3;
        LOG.info("Getting TODO by ID = + " + idRequested);
        GetTodoRequest getTodoRequest = GetTodoRequest.newBuilder().setId(3).build();
        GetTodoReply getTodoReply = blockingStub.get(getTodoRequest);
        LOG.info("Todo (id: " + getTodoReply.getId() + ", description: " + getTodoReply.getDescription() + ")");
        LOG.info("--------------------------------------------------\n");

        LOG.info("Updating TODO by ID = + " + idRequested);
        LOG.info("--------------------------------------------------\n");
        PutTodoRequest putTodoRequest = PutTodoRequest.newBuilder().setId(3).setDescription("Go to Disney Land")
                .build();
        blockingStub.put(putTodoRequest);

        LOG.info("Listing all todos...");
        getTodosReply = blockingStub.getAll(Empty.newBuilder().build());
        getTodosReply.getTodosList().forEach(todo -> LOG.info("Todo (id: " + todo.getId()
                + ", description: " + todo.getDescription() + ")"));
        LOG.info("--------------------------------------------------\n");

        LOG.info("Deleting TODO by ID = " + idRequested);
        LOG.info("--------------------------------------------------\n");
        DeleteTodoRequest deleteTodoRequest = DeleteTodoRequest.newBuilder().setId(3).build();
        blockingStub.delete(deleteTodoRequest);

        LOG.info("Listing all todos...");
        getTodosReply = blockingStub.getAll(Empty.newBuilder().build());
        getTodosReply.getTodosList().forEach(todo -> LOG.info("Todo (id: " + todo.getId()
                + ", description: " + todo.getDescription() + ")"));
        LOG.info("--------------------------------------------------\n");
    }

    private PostTodoRequest createTodo(String description) {
        return PostTodoRequest.newBuilder()
                .setDescription(description)
                .build();
    }

    public static void main(String[] args) throws InterruptedException {
        String target = "localhost:50051";

        ManagedChannel channel = Grpc.newChannelBuilder(target, InsecureChannelCredentials.create())
                .build();

        try {
            TodoGrpcClient client = new TodoGrpcClient(channel);
            client.simulateExampleServerFlow();
        } finally {
            channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
        }
    }
}
