package edu.ubb.weblive.todoapp.service;

public final class TodosManagerFactory {

    private static TodosManager INSTANCE;

    public static TodosManager getTodosManager() {
        if (INSTANCE == null) {
            INSTANCE = new TodosManager();
        }
        return INSTANCE;
    }
}
