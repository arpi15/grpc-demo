package edu.ubb.weblive.todoapp.model;

import java.util.concurrent.atomic.AtomicInteger;

public class Todo {

    private static final AtomicInteger ID_GENERATOR = new AtomicInteger(0);

    private Integer id;
    private String description;

    public Todo() {
    }

    public Todo(String description) {
        this.description = description;

        this.id = ID_GENERATOR.getAndIncrement();
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
