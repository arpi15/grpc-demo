package edu.ubb.weblive.todoapp.service;

import edu.ubb.weblive.todoapp.model.Todo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashSet;
import java.util.Set;

public final class TodosManager {

    private static final Logger LOG = LoggerFactory.getLogger(TodosManager.class);

    private final Set<Todo> todos = new LinkedHashSet<>();

    public Set<Todo> findAll() {
        LOG.info("Finding all todos.");
        return todos;
    }

    public Todo findById(Integer id) {
        LOG.info("Finding todo by ID: " + id + ".");

        return todos.stream()
                .filter(todo -> todo.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public void create(String description) {
        LOG.info("Creating a new todo with description: " + description + ".");
        todos.add(new Todo(description));
    }

    public void update(Integer id, String description) {
        LOG.info("Updating todo with ID: " + id + ".");
        Todo todo = findById(id);
        if (todo != null) {
            todo.setDescription(description);
        }
    }

    public void delete(Integer id) {
        LOG.info("Deleting todo with ID: " + id + ".");
        todos.removeIf(todo -> todo.getId().equals(id));
    }
}
