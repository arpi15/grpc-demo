﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;

namespace TodoServiceWithJson.Services
{
    public class TodoService : Todo.TodoBase
    {

        private readonly Todo.TodoClient client;

        public TodoService() {
            var channel = GrpcChannel.ForAddress("http://localhost:50051");
            client = new Todo.TodoClient(channel);
        }

        public override async Task<GetTodosReply> GetAll(Empty request, ServerCallContext context) {
            var response = await client.GetAllAsync(request);

            Console.WriteLine("Finding all todos.");

            return response;
        }

        public override async Task<GetTodoReply> Get(GetTodoRequest request, ServerCallContext context) {
            var response = await client.GetAsync(request);

            Console.WriteLine("Finding todo by ID: " + request.Id + ".");

            return response;
        }

        public override async Task<Empty> Post(PostTodoRequest request, ServerCallContext context) {
            var response = await client.PostAsync(request);

            Console.WriteLine("Creating a new todo with description: " + request.Description + ".");

            return response;
        }

        public override async Task<Empty> Put(PutTodoRequest request, ServerCallContext context) {
            var response = await client.PutAsync(request);

            Console.WriteLine("Updating todo with ID: " + request.Id + ".");

            return response;
        }

        public override async Task<Empty> Delete(DeleteTodoRequest request, ServerCallContext context) {
            var response = await client.DeleteAsync(request);

            Console.WriteLine("Deleting todo with ID: " + request.Id + ".");

            return response;
        }
    }
}
